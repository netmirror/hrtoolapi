﻿using System.Collections.Generic;
using HR.Tool.API.Domain.Models;
using HR.Tool.API.Domain.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            this._feedbackService = feedbackService;
        }

        [HttpPost("feedback/save-general")]
        public IActionResult SaveGeneralFeedback(GeneralFeedbackRequestModel generalFeedback)
        {
            bool isGeneralFeedbackSaved = _feedbackService.SaveGeneralFeedback(generalFeedback);
            return Ok(isGeneralFeedbackSaved);
        }

        [HttpPost("feedback/save-objective")]
        public IActionResult SaveObjectiveFeedback(ObjectiveFeedbackRequestModel objectiveFeedback)
        {
            bool isObjectiveFeedbackSaved = _feedbackService.SaveObjectiveFeedback(objectiveFeedback);
            return Ok(isObjectiveFeedbackSaved);
        }

        [HttpGet("feedback/general/{userEmail}")]
        public IActionResult GetGeneralFeedbackByUser(string userEmail)
        {
            IList<GeneralFeedbackResponseModel> generalFeedback = _feedbackService.GetGeneralFeedback(userEmail);
            return Ok(generalFeedback);
        }
    }
}
