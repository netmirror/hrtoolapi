﻿using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class ObjectiveController : ControllerBase
    {
        private readonly IObjectiveService _objectiveService;

        public ObjectiveController(IObjectiveService objectiveService)
        {
            _objectiveService = objectiveService;
        }

        [Authorize]
        [HttpGet("objective/user-objectives/{userEmail}")]
        public IActionResult GetUserObjectives(string userEmail)
        {
            var response = _objectiveService.GetUserObjectives(userEmail);
            return Ok(response);
        }

        [Authorize]
        [HttpPost("objective/add-user-objective")]
        public IActionResult AddUserObjective([FromBody] AddUserObjectiveRequestModel request)
        {
            _objectiveService.AddUserObjective(request);
            return Ok();
        }

        [Authorize]
        [HttpPost("objective/complete-user-objective")]
        public IActionResult CompleteUserObjective([FromBody] int objectiveId)
        {
            _objectiveService.CompleteUserObjective(objectiveId);
            return Ok();
        }

        [Authorize]
        [HttpDelete("objective/delete-user-objective")]
        public IActionResult DeleteUserObjective([FromBody] int objectiveId)
        {
            _objectiveService.DeleteUserObjective(objectiveId);
            return Ok();
        }
    }
}
