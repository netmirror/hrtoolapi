﻿using HR.Tool.API.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [Authorize]
        [HttpGet("project/projects")]
        public IActionResult GetProjects()
        {
            var response = _projectService.GetProjects();
            return Ok(response);
        }

        [Authorize]
        [HttpGet("project/user-projects/{userEmail}")]
        public IActionResult GetUserProjects(string userEmail)
        {
            var response = _projectService.GetUserProjects(userEmail);
            return Ok(response);
        }
    }
}
