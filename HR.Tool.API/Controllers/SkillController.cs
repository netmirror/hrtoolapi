﻿using HR.Tool.API.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class SkillController : ControllerBase
    {
        private readonly ISkillService _skillService;

        public SkillController(ISkillService skillService)
        {
            _skillService = skillService;
        }

        [Authorize]
        [HttpGet("skill/skills")]
        public IActionResult GetSkills()
        {
            var response = _skillService.GetSkills();
            return Ok(response);
        }

        [Authorize]
        [HttpGet("skill/user-skills/{userEmail}")]
        public IActionResult GetUserSkills(string userEmail)
        {
            var response = _skillService.GetUserSkills(userEmail);
            return Ok(response);
        }
    }
}
