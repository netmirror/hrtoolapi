﻿using HR.Tool.API.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [Authorize]
        [HttpGet("department/departments")]
        public IActionResult GetDepartments()
        {
            var response = _departmentService.GetDepartments();
            return Ok(response);
        }
    }
}
