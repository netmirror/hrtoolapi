﻿using System.Net;
using System.Threading.Tasks;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Controllers
{
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IAuthService _authService;

        public AuthController(ILogger<AuthController> logger,
            IAuthService authService)
        {
            _logger = logger;
            _authService = authService;
        }

        [AllowAnonymous]
        [HttpPost("auth/register")]
        public async Task<IActionResult> Register(UserModel user)
        {
            bool isRegistered = await _authService.RegisterUser(user);
            return Ok(isRegistered);
        }

        [AllowAnonymous]
        [HttpPost("auth/login")]
        public IActionResult Login(LoginUserModel user)
        {
            AuthResponseModel response = _authService.LoginUser(user).Result;
            if (!string.IsNullOrEmpty(response.Token))
            {
                return Ok(response);
            }

            //Create a global exception handler with Message and StatusCode and register it in Startup.cs
            return Unauthorized(new { Message = "Invalid username or password.", StatusCode = (int)HttpStatusCode.Forbidden });
        }
    }
}
