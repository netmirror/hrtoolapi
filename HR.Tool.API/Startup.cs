using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HR.Tool.API.Data;
using HR.Tool.API.Data.Configurations;
using HR.Tool.API.Data.Dapper;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Data.Repositories;
using HR.Tool.API.Domain.Common;
using HR.Tool.API.Domain.Configurations;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Interfaces.Jwt;
using HR.Tool.API.Domain.Jwt;
using HR.Tool.API.Domain.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace HR.Tool.API
{

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddMvc(options =>
            {
            });

            ConfigureDatabaseContexts(services);

            ConfigureIdentity(services);

            ConfigureAuth(services);

            ConfigureAppSettingsConfigurations(services);

            ConfigureDependencies(services);

            ConfigureSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, HrToolApiDbContext hrToolApiDbContext, IServiceProvider serviceProvider)
        {
            app.UseRouting();
            app.UseSwagger();
            app.UseSwaggerUI(configuration =>
            {
                configuration.SwaggerEndpoint("/swagger/v1/swagger.json", "Hr Tool Api V1");
                //Used by Swagger to use API's root route
                configuration.RoutePrefix = string.Empty;
                configuration.DocumentTitle = "Hr Tool Api Swagger Interface";
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseCors(options => options
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            );

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            AddInitialRoles(serviceProvider).Wait();
        }

        private void ConfigureDependencies(IServiceCollection services)
        {
            //Business Services and helper services
            services.AddScoped(typeof(IAuthService), typeof(AuthService));
            services.AddScoped(typeof(IClaimsConverter), typeof(ClaimsConverter));
            services.AddScoped(typeof(ITokenProvider), typeof(TokenProvider));
            services.AddScoped(typeof(IJwtClaimsExtractor), typeof(JwtClaimsExtractor));

            services.AddScoped(typeof(IHrToolRepository), typeof(HrToolRepository));
            services.AddScoped<IHrToolDataService>(provider => new HrToolDataService(Configuration["ConnectionStrings:HrToolDb"]));

            services.AddScoped(typeof(IUsersService), typeof(UsersService));
            services.AddScoped(typeof(IDepartmentService), typeof(DepartmentService));
            services.AddScoped(typeof(IProjectService), typeof(ProjectService));
            services.AddScoped(typeof(ISkillService), typeof(SkillService));
            services.AddScoped(typeof(IObjectiveService), typeof(ObjectiveService));
            services.AddScoped(typeof(IFeedbackService), typeof(FeedbackService));
        }

        private void ConfigureDatabaseContexts(IServiceCollection services)
        {
            //Configure DbContexts
            services.AddDbContext<HrToolApiDbContext>(option => option.UseSqlServer(Configuration["ConnectionStrings:HrToolDb"]));
        }

        private void ConfigureIdentity(IServiceCollection services)
        {
            //Configure Identity
            services.AddIdentity<User, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
            })
                .AddEntityFrameworkStores<HrToolApiDbContext>()
                .AddDefaultTokenProviders();
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthorization();
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(
                                Encoding.ASCII.GetBytes(Configuration.GetSection("Token:Key").Value)),
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        RoleClaimType = ClaimTypes.Role
                    };
                });
        }

        private void ConfigureAppSettingsConfigurations(IServiceCollection services)
        {
            //Configure config section
            services.Configure<ConnectionStrings>(Configuration.GetSection(nameof(ConnectionStrings)));
            services.Configure<Token>(Configuration.GetSection(nameof(Token)));
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("Hr Tool API V1", new OpenApiInfo
                    {
                        Version = "V1",
                        Title = "Hr Tool API",
                        Description = "Hr Tool API",
                    });
                });
            });
        }

        private async Task AddInitialRoles(IServiceProvider serviceProvider)
        {
            RoleManager<IdentityRole> roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            IList<string> userRoles = UserRoles.GetAll();

            foreach (var userRole in userRoles)
            {
                var roleExist = await roleManager.RoleExistsAsync(userRole);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1
                    await roleManager.CreateAsync(new IdentityRole(userRole));
                }
            }
        }
    }
}
