﻿using System;

namespace HR.Tool.API.Data.Helper
{
    public interface IServiceLocator
    {
        TService GetService<TService>();
        object GetService(Type type);
    }

    public class ServiceLocator : IServiceLocator
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceLocator(IServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }

        public TService GetService<TService>()
        {
            return (TService)_serviceProvider.GetService(typeof(TService));
        }

        public object GetService(Type type)
        {
            return _serviceProvider.GetService(type);
        }
    }
}
