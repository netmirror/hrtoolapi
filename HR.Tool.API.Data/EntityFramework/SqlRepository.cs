﻿using HR.Tool.API.Data.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using HR.Tool.API.Data.Models;

namespace HR.Tool.API.Data.EntityFramework
{
    public class SqlRepository<TContext> : ISqlRepository
        where TContext : IdentityDbContext<User>, new()
    {
        private readonly Lazy<TContext> _dbContext;
        private readonly string _connectionString;

        public SqlRepository(string connectionString)
        {
            _connectionString = connectionString;
            _dbContext = new Lazy<TContext>(BuildReadonlyContext);
        }

        public IQueryable<TEntity> GetEntities<TEntity>() where TEntity : class
        {
            return _dbContext.Value.Set<TEntity>();
        }

        public IUnitOfWork GetUnitOfWork()
        {
            return new SqlUnitOfWork<TContext>(_connectionString);
        }

        public void Dispose()
        {
            if (_dbContext.IsValueCreated)
            {
                _dbContext.Value.Dispose();
            }
        }

        private TContext BuildReadonlyContext()
        {
            var options = new DbContextOptionsBuilder<TContext>().UseSqlServer(_connectionString).Options;
            var result = (TContext)Activator.CreateInstance(typeof(TContext), options);
            if (result != null)
            {
                result.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                result.ChangeTracker.LazyLoadingEnabled = false;
                result.ChangeTracker.AutoDetectChangesEnabled = false;
            }
            return result;
        }
    }
}
