﻿using HR.Tool.API.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace HR.Tool.API.Data.EntityFramework
{
    public class SqlUnitOfWork<TContext> : IUnitOfWork where TContext : IdentityDbContext<User>, new()
    {
        private readonly TContext _context;

        public SqlUnitOfWork(string connectionString)
        {
            var options = new DbContextOptionsBuilder<TContext>().UseSqlServer(connectionString).Options;
            _context = (TContext)Activator.CreateInstance(typeof(TContext), options);
            if (_context != null)
            {
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
                _context.ChangeTracker.LazyLoadingEnabled = false;
                _context.ChangeTracker.AutoDetectChangesEnabled = true;
            }
        }

        public void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void InsertRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            _context.Set<TEntity>().AddRange(entities);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Update(entity);
        }

        public void UpdateRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            _context.Set<TEntity>().UpdateRange(entities);
        }

        public IQueryable<TEntity> GetEntities<TEntity>() where TEntity : class
        {
            return _context.Set<TEntity>();
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void DeleteRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
