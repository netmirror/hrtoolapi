﻿namespace HR.Tool.API.Domain.Configurations
{
    public class Token
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}
