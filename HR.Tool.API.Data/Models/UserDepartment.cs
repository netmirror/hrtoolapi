﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HR.Tool.API.Data.Models
{
    public class UserDepartment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("DepartmentId")]
        public virtual Department Department{ get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
