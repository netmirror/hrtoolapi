﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HR.Tool.API.Data.Models
{
    public class UserProject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("ProjectId")]
        public virtual Project Project{ get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
