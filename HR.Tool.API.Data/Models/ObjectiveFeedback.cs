﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HR.Tool.API.Data.Models
{
    public class ObjectiveFeedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Feedback { get; set; }
        public DateTime DateAdded { get; set; }

        [ForeignKey("ObjectiveId")]
        public virtual Objective Objective { get; set; }
    }
}
