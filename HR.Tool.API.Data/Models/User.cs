﻿using System;
using Microsoft.AspNetCore.Identity;

namespace HR.Tool.API.Data.Models
{
    public class User: IdentityUser
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Position { get; set; }
    }
}
