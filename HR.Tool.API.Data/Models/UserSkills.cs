﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HR.Tool.API.Data.Models
{
    public class UserSkills
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("SkillId")]
        public virtual Skill Skill { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
