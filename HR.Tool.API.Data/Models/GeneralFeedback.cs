﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HR.Tool.API.Data.Models
{
    public class GeneralFeedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Feedback { get; set; }
        public DateTime DateAdded { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
