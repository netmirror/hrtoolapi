﻿using HR.Tool.API.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HR.Tool.API.Data
{
    public class HrToolApiDbContext : IdentityDbContext<User>
    {
        public HrToolApiDbContext() { }
        public HrToolApiDbContext(DbContextOptions<HrToolApiDbContext> options) : base(options) { }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Objective> Objectives { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<UserSkills> UserSkills { get; set; }
        public DbSet<ObjectiveFeedback> ObjectiveFeedbacks { get; set; }
        public DbSet<GeneralFeedback> GeneralFeedbacks { get; set; }
        public DbSet<UserDepartment> UserDepartments { get; set; }
        public DbSet<UserProject> UserProjects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=hrtoolapidb.mssql.somee.com;persist security info=False;initial catalog=hrtoolapidb;user id=calinvlasin_SQLLogin_1;pwd=qir6434zpu");
        }
    }
}
