﻿using HR.Tool.API.Data.Configurations;
using HR.Tool.API.Data.EntityFramework;
using HR.Tool.API.Data.Interfaces;
using Microsoft.Extensions.Options;

namespace HR.Tool.API.Data.Repositories
{
    public class HrToolRepository : SqlRepository<HrToolApiDbContext>, IHrToolRepository
    {
        public HrToolRepository(IOptions<ConnectionStrings> configuration)
            : base(configuration.Value.HrToolDb)
        {
        }
    }
}
