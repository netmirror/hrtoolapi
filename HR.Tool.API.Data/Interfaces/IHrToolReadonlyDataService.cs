﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HR.Tool.API.Data.Interfaces
{
    public interface IHrToolReadonlyDataService
    {
        Task<IEnumerable<TResult>> QueryStoredProcedureAsync<TResult>(string storedProcedureName, object param);
        IList<TResult> QueryStoredProcedure<TResult>(string storedProcedureName, object param);
        void ExecuteStoredProcedure<TParam>(string storedProcedureName, TParam param);
        IList<TResult> Query<TResult>(string query, object param = null);
        int ExecuteRawSql(string query, object param = null);
        MultiQueryResult<T1, T2> MultipleQueryStoredProcedure<T1, T2>(string storedProcedureName, object param = null);
        MultiQueryResult<T1, T2, T3> MultipleQueryStoredProcedure<T1, T2, T3>(string storedProcedureName, object param = null);
    }
}
