﻿using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Data.Common;

namespace HR.Tool.API.Data.Interfaces
{
    public class DataTransaction : IDisposable
    {
        internal readonly SqlConnection Connection;
        internal readonly DbTransaction Transaction;

        internal DataTransaction(string connectionString, IsolationLevel isolationLevel)
        {
            Connection = new SqlConnection(connectionString);
            Connection.Open();
            Transaction = Connection.BeginTransaction(isolationLevel);
        }

        public void Dispose()
        {
            Transaction?.Dispose();
            Connection?.Dispose();
        }

        public void Commit()
        {
            Transaction.Commit();
        }

        public void Rollback()
        {
            Transaction.Rollback();
        }
    }
}