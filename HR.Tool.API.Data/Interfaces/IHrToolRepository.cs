﻿using System;

namespace HR.Tool.API.Data.Interfaces
{
    public interface IHrToolRepository : IQueryDataSource, IDisposable
    {
        IUnitOfWork GetUnitOfWork();
    }
}
