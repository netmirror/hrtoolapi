﻿using System.Linq;

namespace HR.Tool.API.Data.Interfaces
{
    public interface IQueryDataSource
    {
        IQueryable<TEntity> GetEntities<TEntity>()
            where TEntity : class;
    }
}
