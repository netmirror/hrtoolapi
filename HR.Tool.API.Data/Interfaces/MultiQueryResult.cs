﻿using System.Collections.Generic;

namespace HR.Tool.API.Data.Interfaces
{
    public class MultiQueryResult<T1>
    {
        public List<T1> FirstResult { get; set; } = new List<T1>();
    }

    public class MultiQueryResult<T1, T2> : MultiQueryResult<T1>
    {
        public List<T2> SecondResult { get; set; } = new List<T2>();
    }

    public class MultiQueryResult<T1, T2, T3> : MultiQueryResult<T1, T2>
    {
        public List<T3> ThirdResult { get; set; } = new List<T3>();
    }
}