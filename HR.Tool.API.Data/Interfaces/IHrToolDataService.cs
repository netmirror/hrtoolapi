﻿using System.Collections.Generic;
using System.Data;

namespace HR.Tool.API.Data.Interfaces
{
    public interface IHrToolDataService: IHrToolReadonlyDataService
    {
        DataTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        IList<TResult> QueryStoredProcedure<TResult>(string storedProcedureName, object param, DataTransaction transaction);
        void ExecuteStoredProcedure<TParam>(string storedProcedureName, TParam param, DataTransaction transaction);
        IList<TResult> Query<TResult>(string query, DataTransaction transaction);
        int ExecuteRawSql(string query, DataTransaction transaction, object param = null);
    }
}
