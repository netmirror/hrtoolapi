﻿using System;
using System.Collections.Generic;

namespace HR.Tool.API.Data.Interfaces
{
    public interface IUnitOfWork : IQueryDataSource, IDisposable
    {
        void Insert<TEntity>(TEntity entity)
            where TEntity : class;
        void InsertRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;
        void UpdateRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        void Delete<TEntity>(TEntity entity)
             where TEntity : class;
        void DeleteRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        int Commit();
    }
}
