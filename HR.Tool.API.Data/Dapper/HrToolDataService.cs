﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using HR.Tool.API.Data.Interfaces;
using Microsoft.Data.SqlClient;
namespace HR.Tool.API.Data.Dapper
{
    public class HrToolDataService: IHrToolDataService
    {
        protected string ConnectionString { get; set; }

        public HrToolDataService(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public DataTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return new DataTransaction(ConnectionString, isolationLevel);
        }

        public async Task<IEnumerable<TResult>> QueryStoredProcedureAsync<TResult>(string storedProcedureName, object param)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                return await connection.QueryAsync<TResult>(storedProcedureName, param, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            }
        }

        public IList<TResult> QueryStoredProcedure<TResult>(string storedProcedureName, object param)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                return connection.Query<TResult>(storedProcedureName, param, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout)
                    .ToList();
            }
        }

        public IList<TResult> Query<TResult>(string query, object param = null)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                return connection.Query<TResult>(query, param, commandType: CommandType.Text, commandTimeout: connection.ConnectionTimeout).ToList();
            }
        }

        public IList<TResult> QueryStoredProcedure<TResult>(string storedProcedureName, object param,
            DataTransaction transaction)
        {
            if (transaction == null) return QueryStoredProcedure<TResult>(storedProcedureName, param);
            return transaction.Connection.Query<TResult>(storedProcedureName, param, transaction.Transaction,
                commandType: CommandType.StoredProcedure, commandTimeout: transaction.Connection.ConnectionTimeout).ToList();
        }

        public IList<TResult> Query<TResult>(string query, DataTransaction transaction)
        {
            if (transaction == null) return Query<TResult>(query);
            return transaction.Connection.Query<TResult>(query, transaction: transaction.Transaction,
                commandType: CommandType.Text, commandTimeout: transaction.Connection.ConnectionTimeout).ToList();
        }

        public void ExecuteStoredProcedure<TParam>(string storedProcedureName, TParam param)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                connection.Execute(storedProcedureName, param, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
            }
        }

        public void ExecuteStoredProcedure<TParam>(string storedProcedureName, TParam param,
            DataTransaction transaction)
        {
            if (transaction == null)
                ExecuteStoredProcedure(storedProcedureName, param);
            else
                transaction.Connection.Execute(storedProcedureName, param, transaction.Transaction,
                    commandType: CommandType.StoredProcedure, commandTimeout: transaction.Connection.ConnectionTimeout);
        }

        //TODO: This is temporary, until we can move these queries into EF
        public int ExecuteRawSql(string query, object param = null)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                return connection.Execute(query, param, commandType: CommandType.Text, commandTimeout: connection.ConnectionTimeout);
            }
        }

        public int ExecuteRawSql(string query, DataTransaction transaction, object param = null)
        {
            if (transaction == null)
                return ExecuteRawSql(query, param);
            return transaction.Connection.Execute(query, param, transaction.Transaction,
                commandType: CommandType.Text, commandTimeout: transaction.Connection.ConnectionTimeout);
        }

        public MultiQueryResult<T1, T2> MultipleQueryStoredProcedure<T1, T2>(string storedProcedureName, object param = null)
        {
            return MultipleQuerySp(storedProcedureName, param, (grid) => new MultiQueryResult<T1, T2>
            {
                FirstResult = grid.Read<T1>().ToList(),
                SecondResult = grid.Read<T2>().ToList(),
            });
        }
        public MultiQueryResult<T1, T2, T3> MultipleQueryStoredProcedure<T1, T2, T3>(string storedProcedureName, object param = null)
        {
            return MultipleQuerySp(storedProcedureName, param, (grid) => new MultiQueryResult<T1, T2, T3>
            {
                FirstResult = grid.Read<T1>().ToList(),
                SecondResult = grid.Read<T2>().ToList(),
                ThirdResult = grid.Read<T3>().ToList(),
            });
        }

        private T MultipleQuerySp<T>(string spName, object param, Func<SqlMapper.GridReader, T> mapper)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var multi = connection.QueryMultiple(spName, param, commandType: CommandType.StoredProcedure, commandTimeout: connection.ConnectionTimeout);
                return mapper(multi);
            }
        }
    }
}
