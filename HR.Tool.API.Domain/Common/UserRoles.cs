﻿using System.Collections.Generic;

namespace HR.Tool.API.Domain.Common
{
    public class UserRoles
    {
        public const string Admin = "Admin";
        //TODO: This could change and added others.

        public static IList<string> GetAll()
        {
            return new[] { Admin };
        }
    }
}
