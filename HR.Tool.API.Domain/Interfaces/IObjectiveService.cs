﻿using HR.Tool.API.Domain.Models;
using System.Collections.Generic;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface IObjectiveService
    {
        IList<ObjectiveModel> GetUserObjectives(string userEmail);
        void AddUserObjective(AddUserObjectiveRequestModel request);
        void CompleteUserObjective(int objectiveId);
        void DeleteUserObjective(int objectiveId);
    }
}
