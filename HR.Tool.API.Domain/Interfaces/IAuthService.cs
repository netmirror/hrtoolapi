﻿using System.Threading.Tasks;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface IAuthService
    {
        Task<bool> RegisterUser(UserModel addUserModel);
        Task<AuthResponseModel> LoginUser(LoginUserModel userModel);
    }
}
