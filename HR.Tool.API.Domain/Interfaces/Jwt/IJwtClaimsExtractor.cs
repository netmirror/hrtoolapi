﻿using HR.Tool.API.Domain.Jwt;

namespace HR.Tool.API.Domain.Interfaces.Jwt
{
    public interface IJwtClaimsExtractor
    {
        JwtToken ExtractClaims(string token);
    }
}
