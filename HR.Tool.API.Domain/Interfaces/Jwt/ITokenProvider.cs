﻿using System.IdentityModel.Tokens.Jwt;
using HR.Tool.API.Data.Models;

namespace HR.Tool.API.Domain.Interfaces.Jwt
{
    public interface ITokenProvider
    {
        JwtSecurityToken GenerateSecurityToken(User user, string userRole);
    }
}
