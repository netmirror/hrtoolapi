﻿using System;
using System.Collections.Generic;

namespace HR.Tool.API.Domain.Interfaces.Jwt
{
    public interface IClaimsConverter
    {
        string SerializeString(bool? value);
        string SerializeString(string value);
        string SerializeInt(int? value);
        int? DeserializeInt(string value);
        bool DeserializeBool(string value);
        string DeserializeString(string value);
        string SerilizeObject(object value);
        T DeserializeObject<T>(string value);
        IList<string> SerializeArray<T>(IEnumerable<T> arr, Func<T, string> serializeFunc);
        IList<T> DeserializeArray<T>(IEnumerable<string> values, Func<string, T> deserializeFunc);
    }
}
