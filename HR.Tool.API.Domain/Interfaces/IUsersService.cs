﻿using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Models;
using System.Collections.Generic;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface IUsersService
    {
        User GetUser(string email);
        IList<UserModelBase> GetUsers();
    }
}