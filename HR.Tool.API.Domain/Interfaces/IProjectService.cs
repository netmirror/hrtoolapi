﻿using System.Collections.Generic;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface IProjectService
    {
        IList<ProjectModel> GetProjects();
        IList<ProjectModel> GetUserProjects(string userEmail);
    }
}
