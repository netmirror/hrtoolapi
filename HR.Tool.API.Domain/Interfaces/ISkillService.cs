﻿using System.Collections.Generic;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface ISkillService
    {
        IList<SkillModel> GetSkills();
        IList<SkillModel> GetUserSkills(string userEmail);
    }
}
