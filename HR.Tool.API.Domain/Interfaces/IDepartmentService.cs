﻿using System.Collections.Generic;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Interfaces
{
    public interface IDepartmentService
    {
        IList<DepartmentModel> GetDepartments();
    }
}
