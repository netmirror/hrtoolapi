﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class ObjectiveModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        [Required]
        public DateTime CompletedScheduledDate { get; set; }
        [Required]
        public bool IsCompleted { get; set; }
        public List<ObjectiveFeedbackModel> ObjectiveFeedbackItems { get; set; }
    }
}
