﻿using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class LoginUserModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
