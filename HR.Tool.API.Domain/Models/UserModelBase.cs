﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class UserModelBase
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Position { get; set; }
        public IList<int> SkillIds { get; set; }
        public IList<int> ProjectIds { get; set; }
        public IList<int> DepartmentIds { get; set; }
    }
}
