﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class ObjectiveFeedbackModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Feedback { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        [Required]
        public int ObjectiveId { get; set; }
    }
}
