﻿using System;

namespace HR.Tool.API.Domain.Models
{
    public class GeneralFeedbackResponseModel
    {
        public string Feedback { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
