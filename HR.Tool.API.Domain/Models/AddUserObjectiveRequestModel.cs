﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace HR.Tool.API.Domain.Models
{
    public class AddUserObjectiveRequestModel
    {
        [Required]
        public string UserEmail { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime CompletedScheduledDate { get; set; }
    }
}
