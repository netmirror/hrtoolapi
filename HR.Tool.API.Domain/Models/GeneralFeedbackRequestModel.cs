﻿namespace HR.Tool.API.Domain.Models
{
    public class GeneralFeedbackRequestModel
    {
        public string UserEmail { get; set; }
        public string Feedback { get; set; }
    }
}
