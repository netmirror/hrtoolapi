﻿using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class UserModel: UserModelBase
    {
        [Required]
        public string Password { get; set; }
    }
}
