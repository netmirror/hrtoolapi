﻿using System.ComponentModel.DataAnnotations;

namespace HR.Tool.API.Domain.Models
{
    public class SkillModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
