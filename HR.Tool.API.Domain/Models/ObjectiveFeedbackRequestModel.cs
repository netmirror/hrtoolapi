﻿namespace HR.Tool.API.Domain.Models
{
    public class ObjectiveFeedbackRequestModel
    {
        public int ObjectiveId { get; set; }
        public string Feedback { get; set; }
    }
}
