﻿using System;

namespace HR.Tool.API.Domain.Models
{
    public class AuthResponseModel
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
