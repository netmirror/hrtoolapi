﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Configurations;
using HR.Tool.API.Domain.Interfaces.Jwt;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace HR.Tool.API.Domain.Jwt
{
    public class TokenProvider: ITokenProvider
    {
        private readonly IClaimsConverter _claimsConverter;
        private readonly IOptions<Token> _tokenConfiguration;
        private readonly IHrToolRepository _hrToolRepository;
        public TokenProvider(IClaimsConverter claimsConverter, IOptions<Token> tokenConfiguration, IHrToolRepository hrToolRepository)
        {
            _claimsConverter = claimsConverter;
            _tokenConfiguration = tokenConfiguration;
            _hrToolRepository = hrToolRepository;
        }

        public JwtSecurityToken GenerateSecurityToken(User user, string userRole)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Email, user.Email)
            };

            AddClaimRole(userRole, claims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenConfiguration.Value.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //Add hours in config
            var expires = DateTime.UtcNow.AddHours(8);

            using var unitOfWork = _hrToolRepository.GetUnitOfWork();
            var userDepartmentIds = unitOfWork.GetEntities<UserDepartment>().Where(x => x.User.Id == user.Id).Select(x=>x.Id.ToString()).ToList();
            var userSkills = unitOfWork.GetEntities<UserSkills>().Where(x => x.User.Id == user.Id).Select(x => x.Id.ToString()).ToList();
            var userProjects = unitOfWork.GetEntities<UserProject>().Where(x => x.User.Id == user.Id).Select(x => x.Id.ToString()).ToList();

            var token = new JwtSecurityToken(
                //Solve this issue in dependency injection Container - not to have value
                _tokenConfiguration.Value.Issuer,
                _tokenConfiguration.Value.Audience,
                claims,
                expires: expires,
                signingCredentials: creds
            )
            {
                Payload =
                {
                    [nameof(JwtToken.FirstName)] =  _claimsConverter.SerializeString(user.FirstName),
                    [nameof(JwtToken.LastName)] =  _claimsConverter.SerializeString(user.LastName),
                    [nameof(JwtToken.Email)] = _claimsConverter.SerializeString(user.Email),
                    [nameof(JwtToken.PhoneNumber)] = _claimsConverter.SerializeString(user.PhoneNumber),
                    [nameof(JwtToken.Position)] = _claimsConverter.SerializeString(user.Position),
                    [nameof(JwtToken.DateOfBirth)] = _claimsConverter.SerializeString(user.DateOfBirth.ToString()),
                    [nameof(JwtToken.Departments)] = _claimsConverter.SerializeArray(userDepartmentIds, _claimsConverter.SerializeString),
                    [nameof(JwtToken.Skills)] = _claimsConverter.SerializeArray(userSkills, _claimsConverter.SerializeString),
                    [nameof(JwtToken.Projects)] = _claimsConverter.SerializeArray(userProjects, _claimsConverter.SerializeString),
                }
            };
            return token;
        }

        private static void AddClaimRole(string role, IList<Claim> claims)
        {
            if (!string.IsNullOrEmpty(role))
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
        }
    }
}
