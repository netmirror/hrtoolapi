﻿using System;
using System.Collections.Generic;

namespace HR.Tool.API.Domain.Jwt
{
    public class JwtToken
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Position { get; set; }
        public DateTime DateOfBirth { get; set; }
        public IList<int> Skills { get; set; }
        public IList<int> Departments { get; set; }
        public IList<int> Projects { get; set; }
    }
}
