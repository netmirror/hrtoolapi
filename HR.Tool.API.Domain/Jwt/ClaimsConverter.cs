﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HR.Tool.API.Domain.Interfaces.Jwt;
using Newtonsoft.Json;

namespace HR.Tool.API.Domain.Jwt
{
    public class ClaimsConverter : IClaimsConverter
    {
        public string SerializeString(bool? value)
        {
            return value.HasValue ? value.ToString().ToLower() : null;
        }
        public string SerializeString(string value)
        {
            return value == null ? null : value.Trim();
        }

        public string SerializeInt(int? value)
        {
            return value.HasValue ? value.ToString() : "";
        }

        public int? DeserializeInt(string value)
        {
            bool tryParse = int.TryParse(value, out var result);
            if (!tryParse) return null;
            return result;
        }

        public bool DeserializeBool(string value)
        {
            bool tryParse = bool.TryParse(value, out var result);
            return tryParse ? result : tryParse;
        }
        public string DeserializeString(string value)
        {
            return value?.Trim();
        }

        public string SerilizeObject(object value)
        {
            return value != null ? JsonConvert.SerializeObject(value) : null;
        }

        public T DeserializeObject<T>(string value)
        {
            return string.IsNullOrEmpty(value) ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public IList<string> SerializeArray<T>(IEnumerable<T> arr, Func<T, string> serializeFunc)
        {
            var result = arr?.Select(serializeFunc).ToList();
            return result;
        }

        public IList<T> DeserializeArray<T>(IEnumerable<string> values, Func<string, T> deserializeFunc)
        {
            var result = values?.Select(deserializeFunc).ToList() ?? new List<T>();
            return result;
        }
    }
}
