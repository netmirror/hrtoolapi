﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using HR.Tool.API.Domain.Interfaces.Jwt;
using Newtonsoft.Json.Linq;

namespace HR.Tool.API.Domain.Jwt
{
    public class JwtClaimsExtractor : IJwtClaimsExtractor
    {
        private readonly JwtSecurityTokenHandler _tokenHandler;
        private readonly IClaimsConverter _converter;

        public JwtClaimsExtractor(IClaimsConverter converter)
        {
            this._converter = converter;
            _tokenHandler = new JwtSecurityTokenHandler();
        }

        public JwtToken ExtractClaims(string token)
        {
            var payload = _tokenHandler.ReadJwtToken(token).Payload;
            JwtToken result = new JwtToken
            {
                Email = DeserializeClaim(payload, nameof(result.Email), val => _converter.DeserializeString(val)),
                FirstName = DeserializeClaim(payload, nameof(result.FirstName), _converter.DeserializeString),
                LastName = DeserializeClaim(payload, nameof(result.LastName), _converter.DeserializeString),
                //Role = DeserializeClaim(payload, nameof(result.Role), _converter.DeserializeString),
                //Department = DeserializeClaim(payload, nameof(result.Department), _converter.DeserializeString)
            };
            return result;
        }

        private IList<T> DeserializeArrayClaim<T>(JwtPayload payload, string type, Func<string, T> deserializeFunc)
        {
            if (!payload.ContainsKey(type) || payload[type] == null)
                return new List<T>();
            IEnumerable<string> values = ((JArray)payload[type]).Select(x => x.ToString()).ToList();
            return _converter.DeserializeArray(values, deserializeFunc);
        }

        private T DeserializeClaim<T>(JwtPayload payload, string type, Func<string, T> deserializeFunc)
        {
            if (!payload.ContainsKey(type) || payload[type] == null)
                return default(T);
            return deserializeFunc(payload[type]?.ToString());
        }
    }
}
