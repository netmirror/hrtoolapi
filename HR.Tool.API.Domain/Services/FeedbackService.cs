﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Services
{
    public interface IFeedbackService
    {
        bool SaveGeneralFeedback(GeneralFeedbackRequestModel generalFeedback);
        IList<GeneralFeedbackResponseModel> GetGeneralFeedback(string userEmail);
        bool SaveObjectiveFeedback(ObjectiveFeedbackRequestModel objectiveFeedback);
    }

    public class FeedbackService: IFeedbackService
    {
        private readonly IHrToolRepository _hrToolRepository;

        public FeedbackService(IHrToolRepository hrToolRepository)
        {
            _hrToolRepository = hrToolRepository;
        }

        public bool SaveGeneralFeedback(GeneralFeedbackRequestModel generalFeedback)
        {
            using var unitOfWork = _hrToolRepository.GetUnitOfWork();
            User user = unitOfWork.GetEntities<User>().FirstOrDefault(x => x.Email == generalFeedback.UserEmail);
            GeneralFeedback feedback = new GeneralFeedback
            {
                DateAdded = DateTime.Now,
                Feedback = generalFeedback.Feedback,
                User = user,
            };
            unitOfWork.Insert(feedback);
            return unitOfWork.Commit() > 0;
        }

        public IList<GeneralFeedbackResponseModel> GetGeneralFeedback(string userEmail)
        {
            using var unitOfWork = _hrToolRepository.GetUnitOfWork();
            User user = unitOfWork.GetEntities<User>().FirstOrDefault(x => x.Email == userEmail);
            var generalFeedback = unitOfWork.GetEntities<GeneralFeedback>().Where(x => x.User == user).Select(feed =>
                new GeneralFeedbackResponseModel()
                {
                    DateAdded = feed.DateAdded,
                    Feedback = feed.Feedback,
                }).ToList();
            return generalFeedback;
        }

        public bool SaveObjectiveFeedback(ObjectiveFeedbackRequestModel objectiveFeedbackArg)
        {
            using var unitOfWork = _hrToolRepository.GetUnitOfWork();
            Objective objective = unitOfWork.GetEntities<Objective>().FirstOrDefault(x => x.Id == objectiveFeedbackArg.ObjectiveId);
            var objectiveFeedback = new ObjectiveFeedback()
            {
                DateAdded = DateTime.Now,
                Feedback = objectiveFeedbackArg.Feedback,
                Objective = objective
            };
            unitOfWork.Insert(objectiveFeedback);
            return unitOfWork.Commit() > 0;
        }
    }
}
