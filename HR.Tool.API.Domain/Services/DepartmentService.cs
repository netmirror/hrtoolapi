﻿using System;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Domain.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IHrToolRepository _hrToolRepository;
        private readonly ILogger<DepartmentService> _logger;

        public DepartmentService(
            IHrToolRepository hrToolRepository,
            ILogger<DepartmentService> logger)
        {
            _hrToolRepository = hrToolRepository;
            _logger = logger;
        }

        public IList<DepartmentModel> GetDepartments()
        {
            try
            {
                var departments = _hrToolRepository.GetEntities<Department>().Select(d => new DepartmentModel
                {
                    Id = d.Id,
                    Name = d.Name,
                }).ToList();
                return departments;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return new List<DepartmentModel>();
        }
    }
}
