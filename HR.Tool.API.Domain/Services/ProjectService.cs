﻿using System;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IHrToolRepository _hrToolRepository;
        private readonly IUsersService _userService;
        private readonly ILogger<ProjectService> _logger;

        public ProjectService(
            IHrToolRepository hrToolRepository,
            IUsersService userService,
            ILogger<ProjectService> logger)
        {
            _hrToolRepository = hrToolRepository;
            _userService = userService;
            _logger = logger;
        }

        public IList<ProjectModel> GetProjects()
        {
            try
            {
                var projects = _hrToolRepository.GetEntities<Project>().Select(p => new ProjectModel
                {
                    Id = p.Id,
                    Name = p.Name,
                }).ToList();
                return projects;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return new List<ProjectModel>();
        }

        public IList<ProjectModel> GetUserProjects(string userEmail)
        {
            try
            {
                var user = _userService.GetUser(userEmail);
                if (user != null)
                {
                    var projects = _hrToolRepository.GetEntities<UserProject>().Include(up => up.Project).Include(up => up.User).Where(up => up.User.Id == user.Id).Select(up => new ProjectModel
                    {
                        Id = up.Project.Id,
                        Name = up.Project.Name,
                    }).ToList();
                    return projects;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return null;
        }
    }
}
