﻿using System;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Domain.Services
{
    public class SkillService : ISkillService
    {
        private readonly IHrToolRepository _hrToolRepository;
        private readonly IUsersService _userService;
        private readonly ILogger<SkillService> _logger;

        public SkillService(
            IHrToolRepository hrToolRepository,
            IUsersService userService,
            ILogger<SkillService> logger)
        {
            _hrToolRepository = hrToolRepository;
            _userService = userService;
            _logger = logger;
        }

        public IList<SkillModel> GetSkills()
        {
            try
            {
                var skills = _hrToolRepository.GetEntities<Skill>().Select(d => new SkillModel
                {
                    Id = d.Id,
                    Name = d.Name,
                }).ToList();
                return skills;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return new List<SkillModel>();
        }

        public IList<SkillModel> GetUserSkills(string userEmail)
        {
            try
            {
                var user = _userService.GetUser(userEmail);
                if (user != null)
                {
                    var skills = _hrToolRepository.GetEntities<UserSkills>().Include(us => us.Skill).Include(us => us.User).Where(us => us.User.Id == user.Id).Select(us => new SkillModel
                    {
                        Id = us.Skill.Id,
                        Name = us.Skill.Name,
                    }).ToList();
                    return skills;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return new List<SkillModel>();
        }
    }
}
