﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Interfaces.Jwt;
using HR.Tool.API.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Domain.Services
{
    public class AuthService: IAuthService
    {
        private readonly UserManager<User> _userManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly ITokenProvider _tokenProvider;
        private readonly ILogger<AuthService> _logger;
        private readonly IHrToolRepository _hrToolRepository;

        public AuthService(
            UserManager<User> userManager,
            IPasswordHasher<User> passwordHasher,
            ITokenProvider tokenProvider,
            ILogger<AuthService> logger,
            IHrToolRepository hrToolRepository)
        {
            _userManager = userManager;
            _passwordHasher = passwordHasher;
            _tokenProvider = tokenProvider;
            _logger = logger;
            _hrToolRepository = hrToolRepository;
        }

        public async Task<AuthResponseModel> LoginUser(LoginUserModel userModel)
        {
            var loginUserResponseModel = new AuthResponseModel();
            try
            {
                var loggedUser = await _userManager.FindByNameAsync(userModel.Email);
                if (loggedUser != null)
                {
                    if (_passwordHasher.VerifyHashedPassword(loggedUser, loggedUser.PasswordHash, userModel.Password) == PasswordVerificationResult.Success)
                    {
                        IList<string> userRole = await _userManager.GetRolesAsync(loggedUser);
                        var token = _tokenProvider.GenerateSecurityToken(loggedUser, userRole.FirstOrDefault());

                        loginUserResponseModel = new AuthResponseModel
                        {
                            Token = $"bearer {new JwtSecurityTokenHandler().WriteToken(token)}",
                            Expiration = token.ValidTo
                        };
                        return loginUserResponseModel;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return loginUserResponseModel;
        }

        public async Task<bool> RegisterUser(UserModel addUserModel)
        {
            try
            {
                var user = new User
                {
                    Email = addUserModel.Email,
                    UserName = addUserModel.Email,
                    FirstName = addUserModel.FirstName,
                    LastName = addUserModel.LastName,
                    DateOfBirth = addUserModel.DateOfBirth,
                    PhoneNumber = addUserModel.PhoneNumber,
                    Position = addUserModel.Position
                };

                IdentityResult result = await _userManager.CreateAsync(user, addUserModel.Password);
                if (result.Succeeded)
                {
                    using var unitOfWork = _hrToolRepository.GetUnitOfWork();

                    var addedUser = unitOfWork.GetEntities<User>().FirstOrDefault();
                    var projects = unitOfWork.GetEntities<Project>();
                    var departments = unitOfWork.GetEntities<Department>();
                    var skills = unitOfWork.GetEntities<Skill>();

                    foreach (var projectId in addUserModel.ProjectIds)
                    {
                        var userProject = new UserProject
                        {
                            User = addedUser,
                            Project = projects.FirstOrDefault(x=>x.Id == projectId)
                        };
                        unitOfWork.Insert(userProject);
                    }

                    foreach (var departmentId in addUserModel.DepartmentIds)
                    {
                        var userDepartment = new UserDepartment
                        {
                            User = addedUser,
                            Department = departments.FirstOrDefault(x => x.Id == departmentId)
                        };
                        unitOfWork.Insert(userDepartment);
                    }

                    foreach (var skillId in addUserModel.SkillIds)
                    {
                        var userSkill = new UserSkills
                        {
                            User = addedUser,
                            Skill = skills.FirstOrDefault(x => x.Id == skillId)
                        };
                        unitOfWork.Insert(userSkill);
                    }
                    unitOfWork.Commit();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
            
        }
    }
}
