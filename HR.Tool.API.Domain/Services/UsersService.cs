﻿using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;

namespace HR.Tool.API.Domain.Services
{
    public class UsersService: IUsersService
    {
        private readonly IHrToolRepository _hrToolRepository;

        public UsersService(IHrToolRepository hrToolRepository)
        {
            _hrToolRepository = hrToolRepository;
        }

        public User GetUser(string email)
        {
            return _hrToolRepository.GetEntities<User>().FirstOrDefault(x => x.Email == email);
        }

        public IList<UserModelBase> GetUsers()
        {
            using var unitOfWork = _hrToolRepository.GetUnitOfWork();

            var dbUsers = unitOfWork.GetEntities<User>().ToList();
            var projects = unitOfWork.GetEntities<UserProject>().ToList();
            var departments = unitOfWork.GetEntities<UserDepartment>().ToList();
            var skills = unitOfWork.GetEntities<UserSkills>().ToList();

            var users = dbUsers.Select(user => new UserModelBase
            {
                LastName = user.LastName,
                FirstName = user.FirstName,
                Email = user.Email,
                Position = user.Position,
                DateOfBirth = user.DateOfBirth,
                PhoneNumber = user.PhoneNumber,
                ProjectIds = projects.Where(x=>x.User.Id == user.Id).Select(d=>d.Id).ToList(),
                DepartmentIds = departments.Where(x => x.User.Id == user.Id).Select(d => d.Id).ToList(),
                SkillIds = skills.Where(x => x.User.Id == user.Id).Select(d => d.Id).ToList()
            }).ToList();
            return users;
        }
    }
}
