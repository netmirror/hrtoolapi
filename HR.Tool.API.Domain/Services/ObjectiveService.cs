﻿using System;
using System.Collections.Generic;
using System.Linq;
using HR.Tool.API.Data.Interfaces;
using HR.Tool.API.Data.Models;
using HR.Tool.API.Domain.Interfaces;
using HR.Tool.API.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HR.Tool.API.Domain.Services
{
    public class ObjectiveService : IObjectiveService
    {
        private readonly IHrToolRepository _hrToolRepository;
        private readonly IUsersService _userService;
        private readonly ILogger<ObjectiveService> _logger;

        public ObjectiveService(
            IHrToolRepository hrToolRepository,
            IUsersService userService,
            ILogger<ObjectiveService> logger)
        {
            _hrToolRepository = hrToolRepository;
            _userService = userService;
            _logger = logger;
        }

        public IList<ObjectiveModel> GetUserObjectives(string userEmail)
        {
            try
            {
                var user = _userService.GetUser(userEmail);
                if (user != null)
                {
                    var objectives = _hrToolRepository.GetEntities<Objective>().Include(o => o.User).Where(o => o.User.Id == user.Id).Select(o => new ObjectiveModel
                    {
                        Id = o.Id,
                        Title = o.Title,
                        Description = o.Description,
                        DateAdded = o.DateAdded,
                        CompletedScheduledDate = o.CompletedScheduledDate,
                        IsCompleted = o.IsCompleted,
                        ObjectiveFeedbackItems = _hrToolRepository.GetEntities<ObjectiveFeedback>().Include(of => of.Objective).Where(of => of.Objective.Id == o.Id).Select(of => new ObjectiveFeedbackModel
                        {
                            Id = of.Id,
                            DateAdded = of.DateAdded,
                            ObjectiveId = o.Id,
                            Feedback = of.Feedback
                        }).ToList()
                    }).ToList();
                    return objectives;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return new List<ObjectiveModel>();
        }

        public void AddUserObjective(AddUserObjectiveRequestModel request)
        {
            try
            {
                using var unitOfWork = _hrToolRepository.GetUnitOfWork();
                var user = unitOfWork.GetEntities<User>().FirstOrDefault(user => user.Email == request.UserEmail);
                if (user != null)
                {
                    var userObjective = new Objective
                    {
                        Title = request.Title,
                        Description = request.Description,
                        IsCompleted = false,
                        DateAdded = DateTime.Now,
                        CompletedScheduledDate = request.CompletedScheduledDate,
                        User = user,
                    };
                    unitOfWork.Insert(userObjective);
                    unitOfWork.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void CompleteUserObjective(int objectiveId)
        {
            try
            {
                using var unitOfWork = _hrToolRepository.GetUnitOfWork();
                var objective = unitOfWork.GetEntities<Objective>().FirstOrDefault(o => o.Id == objectiveId);
                if (objective != null)
                {
                    objective.IsCompleted = true;
                    unitOfWork.Commit();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void DeleteUserObjective(int objectiveId)
        {
            try
            {
                using (var unitOfWork = _hrToolRepository.GetUnitOfWork())
                {
                    var objective = unitOfWork.GetEntities<Objective>().FirstOrDefault(o => o.Id == objectiveId);
                    if (objective != null)
                    {
                        unitOfWork.Delete(objective);
                        unitOfWork.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
